---
title: "Preparação do ambiente"
author: "Gabriel Chaves, Isabela Plessim, Lucas Andrade, Marco Vídero e Mirela Casado"
---
# Tutorial

Necessitaremos dos seguintes softwares para preparação do ambiente:

- Node.js
- Visual Studio Code
- Expo

Esse tutorial apresentará os passos necessários para instalação e configuração das dependências.

!!! info
    Este passo a passo foi elaborado num computador com sistema operacional Windows 10.

#### Node.js

A instalação do Node.js é simples. Basicamente se limita a clicar em ++"Next"++, ++"Next"++, ... e ++"Finish"++.

1. Acesse o site do [Node.js](https://nodejs.org/en/download/) e faça o download do arquivo de instalação;
![Página de download do Node.js](assets/downloadNodeJS-01.png)
2. Execute o instalador (node-v14.17.3-x64.msi). Na janela que se abrirá, clique no botão ++"Next"++;
![installNodeJS-01.png](assets/installNodeJS-01.png)
3. Na tela seguinte, marque o box :material-checkbox-outline: _"I accept the terms in the License Agreement"_ e clique no botão ++"Next"++;
![installNodeJS-02.png](assets/installNodeJS-02.png)
4. Na tela seguinte, clique no botão ++"Next"++;
![installNodeJS-03.png](assets/installNodeJS-03.png)
5. Mais uma vez, clique no botão ++"Next"++;
![installNodeJS-04.png](assets/installNodeJS-04.png)
6. Marque _":material-checkbox-outline: Automatically install the necessary tools..."_ e clique no botão ++"Next"++;
![installNodeJS-05.png](assets/installNodeJS-05.png)
7. Clique no botão ++"Install"++;
![installNodeJS-06.png](assets/installNodeJS-06.png)
8. Será solicitada elevação de privilégio. Clique no botão ++"Sim"++ ou forneça a senha de administrador para permitir a instalação;
![installNodeJS-07.png](assets/installNodeJS-07.png)
9. Após a conclusão da instalação do Node.js, clique em ++"Finish"++;
![installNodeJS-08.png](assets/installNodeJS-08.png)
10. Aparecerá uma janela do _Prompt de Comando_ para instalação das ferramentas adicionais. Pressione qualquer tecla para continuar;
![installNodeJS-09.png](assets/installNodeJS-09.png)
11. Na tela seguinte, pressione novamente qualquer tecla para continuar;
![installNodeJS-10.png](assets/installNodeJS-10.png)
12. Mais uma vez, será solicitada elevação de privilégio. Clique no botão ++"Sim"++ ou forneça a senha de administrador para permitir a instalação;
13. Aparecerá uma janela do _PowerShell_ para instalação de outras dependências. Pressione ++enter++ para continuar;
![installNodeJS-11.png](assets/installNodeJS-11.png)

#### Visual Studio Code

